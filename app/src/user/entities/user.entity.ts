import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { PaymentInformation } from '@/payment-information/entities/payment-information.entity';
import { Ride } from '@/ride/entities/ride.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';
import { Role } from '@/role/entities/role.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToOne,
  OneToMany,
} from 'typeorm';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true, nullable: false })
  email: string;

  @Column({ nullable: false })
  password: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @Column()
  roleId: number;

  @ManyToOne(() => Role, (role) => role.users)
  role: Role;

  @OneToOne(
    () => DriverInformation,
    (driverInformation) => driverInformation.user,
  )
  driverInformation: DriverInformation;

  @OneToOne(() => RiderInformation, (riderInformation) => riderInformation.user)
  riderInformation: RiderInformation;

  @OneToOne(
    () => PaymentInformation,
    (paymentInformation) => paymentInformation.user,
  )
  paymentInformation: PaymentInformation;

  @OneToMany(() => Ride, (ride) => ride.driver)
  rides: Ride[];
}
