import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';
import { RoleEnum } from '@/role/entities/role.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(DriverInformation)
    private driverInformationRepository: Repository<DriverInformation>,
    @InjectRepository(RiderInformation)
    private riderInformationRepository: Repository<RiderInformation>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    let userSaved = await this.userRepository.save(createUserDto);

    if (createUserDto.roleId === RoleEnum.driver) {
      let driverInformation = this.driverInformationRepository.create({
        first_name: createUserDto.first_name,
        last_name: createUserDto.last_name,
        mobile_phone: createUserDto.mobile_phone,
        location: `${this.generateRandomNumber(-90, 90)},${this.generateRandomNumber(-180, 180)}`,
      });

      driverInformation.user = userSaved;
      await this.driverInformationRepository.save(driverInformation);
    }

    if (createUserDto.roleId === RoleEnum.rider) {
      let riderInformation = this.riderInformationRepository.create({
        first_name: createUserDto.first_name,
        last_name: createUserDto.last_name,
        mobile_phone: createUserDto.mobile_phone,
        location: `${this.generateRandomNumber(-90, 90)},${this.generateRandomNumber(-180, 180)}`,
      });

      riderInformation.user = userSaved;
      await this.riderInformationRepository.save(riderInformation);
    }

    return userSaved;
  }

  generateRandomNumber(min, max) {
    const randomDecimal = Math.random();
    const randomNumber = randomDecimal * (max - min) + min;
    return Math.floor(randomNumber);
  }

  findByEmail(email: string) {
    return this.userRepository.findOneBy({ email });
  }
}
