export class CreateUserDto {
  email: string;
  password: string;
  roleId: number;
  first_name: string;
  last_name: string;
  mobile_phone: string;
}
