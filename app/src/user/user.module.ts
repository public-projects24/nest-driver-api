import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { RoleModule } from '@/role/role.module';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, DriverInformation, RiderInformation]),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
