import { Module } from '@nestjs/common';

import { RoleModule } from './role/role.module';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { WelcomeModule } from './welcome/welcome.module';
import { dataSourceOptions } from 'database/data-source';
import { DriverInformationModule } from './driver-information/driver-information.module';
import { PaymentMethodModule } from './payment-method/payment-method.module';
import { RiderInformationModule } from './rider-information/rider-information.module';
import { WompiModule } from './wompi/wompi.module';
import { PaymentInformationModule } from './payment-information/payment-information.module';
import { RideStatusModule } from './ride-status/ride-status.module';
import { RideModule } from './ride/ride.module';
import { SeedersModule } from './seeders/seeders.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(dataSourceOptions),
    RoleModule,
    UserModule,
    AuthModule,
    WelcomeModule,
    DriverInformationModule,
    PaymentMethodModule,
    RiderInformationModule,
    WompiModule,
    PaymentInformationModule,
    RideStatusModule,
    RideModule,
    SeedersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
