import { Module } from '@nestjs/common';
import { SeedersService } from './seeders.service';
import { SeedersController } from './seeders.controller';
import { Role } from '@/role/entities/role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentMethod } from '@/payment-method/entities/payment-method.entity';
import { RideStatus } from '@/ride-status/entities/ride-status.entity';
import { User } from '@/user/entities/user.entity';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Role,
      PaymentMethod,
      RideStatus,
      User,
      DriverInformation,
      RiderInformation,
    ]),
  ],
  controllers: [SeedersController],
  providers: [SeedersService],
})
export class SeedersModule {}
