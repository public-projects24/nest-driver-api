import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { PaymentMethod } from '@/payment-method/entities/payment-method.entity';
import { RideStatus } from '@/ride-status/entities/ride-status.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';
import { Role, RoleEnum } from '@/role/entities/role.entity';
import { User } from '@/user/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { faker } from '@faker-js/faker';

@Injectable()
export class SeedersService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
    @InjectRepository(PaymentMethod)
    private paymentMethodRepository: Repository<PaymentMethod>,
    @InjectRepository(RideStatus)
    private rideStatusRepository: Repository<RideStatus>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(DriverInformation)
    private driverInformationRepository: Repository<DriverInformation>,
    @InjectRepository(RiderInformation)
    private riderInformationRepository: Repository<RiderInformation>,
  ) {}

  async create() {
    // Create roles
    if ((await this.roleRepository.count()) === 0) {
      await this.roleRepository.save({ name: 'driver' });
      await this.roleRepository.save({ name: 'rider' });
    }
    // Create payment methods
    if ((await this.paymentMethodRepository.count()) === 0) {
      await this.paymentMethodRepository.save({ name: 'card' });
      await this.paymentMethodRepository.save({ name: 'nequi' });
    }
    // Create ride statuses
    if ((await this.rideStatusRepository.count()) === 0) {
      await this.rideStatusRepository.save({ name: 'assigned' });
      await this.rideStatusRepository.save({ name: 'finished' });
    }

    // Create users
    if ((await this.userRepository.count()) === 0) {
      await this.createDriverUsers();
      await this.createRiderUsers();
    }

    return { message: 'seeders created' };
  }

  async createDriverUsers() {
    await this.createDriver(`driver@gmail.com`);
    for (let i = 1; i <= 10; i++) {
      await this.createDriver(`driver${i}@gmail.com`);
    }
  }
  async createDriver(email: string) {
    let userSaved = await this.userRepository.save({
      email: email,
      password: '123456',
      roleId: RoleEnum.driver,
    });

    let driverInformation = this.driverInformationRepository.create({
      first_name: faker.person.firstName(),
      last_name: faker.person.lastName(),
      mobile_phone: faker.phone.imei(),
      location: '4.757597868068788,-74.09069447116147',
    });

    driverInformation.user = userSaved;
    await this.driverInformationRepository.save(driverInformation);
  }
  async createRiderUsers() {
    await this.createRider(`rider@gmail.com`);
    for (let i = 1; i <= 10; i++) {
      await this.createRider(`rider${i}@gmail.com`);
    }
  }
  async createRider(email: string) {
    let userSaved = await this.userRepository.save({
      email: email,
      password: '123456',
      roleId: RoleEnum.rider,
    });

    let riderInformation = this.riderInformationRepository.create({
      first_name: faker.person.firstName(),
      last_name: faker.person.lastName(),
      mobile_phone: faker.phone.imei(),
      location: '4.757597868068788,-74.09069447116147',
    });

    riderInformation.user = userSaved;
    await this.riderInformationRepository.save(riderInformation);
  }
}
