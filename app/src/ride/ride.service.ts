import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { CreateRideDto } from './dto/create-ride.dto';
import { UpdateRideDto } from './dto/update-ride.dto';
import { User } from '@/user/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoleEnum } from '@/role/entities/role.entity';
import { Ride } from './entities/ride.entity';
import { RideStatusEnum } from '@/ride-status/entities/ride-status.entity';
import { WompiService } from '@/wompi/wompi.service';

@Injectable()
export class RideService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Ride) private rideRepository: Repository<Ride>,
    private readonly wompiService: WompiService,
  ) {}
  async create(
    createRideDto: CreateRideDto,
    user: { id: number; email: string },
  ) {
    const userFound = await this.userRepository.findOne({
      where: { id: user.id },
    });

    if (userFound.roleId !== RoleEnum.rider) {
      throw new UnauthorizedException('this user cannot request a ride');
    }

    const randomDriver = await this.userRepository
      .createQueryBuilder('user')
      .select(['user', 'driverInformation'])
      .innerJoin('user.driverInformation', 'driverInformation')
      .where('user.roleId = :roleId', { roleId: 1 })
      .orderBy('rand()')
      .getOne();

    const ride = await this.rideRepository.create();
    ride.initial_location = createRideDto.current_location;
    ride.rideStatusId = RideStatusEnum.assigned;
    ride.driverId = randomDriver.id;
    ride.riderId = userFound.id;
    const rideSaved = await this.rideRepository.save(ride);

    return {
      message: 'ride assigned',
      ride: {
        current_location: rideSaved.initial_location,
        ride_id: rideSaved.id,
      },
      driver: {
        id: randomDriver.id,
        email: randomDriver.email,
        name: `${randomDriver.driverInformation.first_name} ${randomDriver.driverInformation.last_name}`,

        phone: randomDriver.driverInformation.mobile_phone,
      },
    };
  }

  async update(
    id: number,
    updateRideDto: UpdateRideDto,
    user: { id: number; email: string },
  ) {
    const userFound = await this.userRepository.findOneBy({ id: user.id });

    if (userFound.roleId !== RoleEnum.driver) {
      throw new UnauthorizedException('this user cannot finish a ride');
    }

    const rideFound = await this.rideRepository.findOne({
      where: { id: id, driverId: userFound.id },
    });

    if (!rideFound) {
      throw new NotFoundException(
        'This ride has not been found associated with this user',
      );
    }

    if (rideFound.rideStatusId === RideStatusEnum.finished) {
      return { message: 'This ride has already been finished and payed' };
    }

    // Finish ride
    rideFound.rideStatusId = RideStatusEnum.finished;
    rideFound.final_location = `${updateRideDto.lat},${updateRideDto.long}`;
    this.rideRepository.save(rideFound);

    // Get the total price of the ride
    const initialLocationArray = rideFound.initial_location.split(',');
    const iLat = initialLocationArray[0];
    const iLong = initialLocationArray[1];

    const fLat = updateRideDto.lat;
    const fLong = updateRideDto.long;

    const distance = this.calculateDistance(iLat, iLong, fLat, fLong);
    const distancePrice = distance * 1000;

    const baseFeed = 3500;
    const totalPrice = Math.round(distancePrice + baseFeed) * 100;

    // Create transaction
    const riderUser = await this.userRepository.findOne({
      where: { id: rideFound.riderId },
      relations: ['riderInformation', 'paymentInformation'],
    });

    const reference = Math.random().toString(36).substring(2, 7);

    const responsePaymentSource = await this.wompiService.createTransaction(
      totalPrice,
      riderUser.email,
      riderUser.riderInformation.mobile_phone,
      riderUser.riderInformation.first_name +
        riderUser.riderInformation.last_name,
      riderUser.paymentInformation.token_source_id,
      reference,
    );

    if (responsePaymentSource.data) {
      const transactionId = responsePaymentSource.data.id;
      const transactionStatus = responsePaymentSource.data.status;

      return {
        message: `ride finished`,
        initial_location: rideFound.initial_location,
        final_location: rideFound.final_location,
        distance: distance,
        total_price: totalPrice,
        transactionId: transactionId,
        transactionStatus: transactionStatus,
      };
    }

    throw new UnprocessableEntityException(
      'No response received from transaction in wompi',
    );
  }

  calculateDistance(lat1, lon1, lat2, lon2) {
    const R = 6371e3; // Earth's radius in meters

    const φ1 = this.toRadians(lat1);
    const φ2 = this.toRadians(lat2);
    const Δφ = φ2 - φ1;
    const λ1 = this.toRadians(lon1);
    const λ2 = this.toRadians(lon2);
    const Δλ = λ2 - λ1;

    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const distance = R * c;

    return Math.round(distance);
  }

  toRadians(degrees) {
    return (degrees * Math.PI) / 180;
  }
}
