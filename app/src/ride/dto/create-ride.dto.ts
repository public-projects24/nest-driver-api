import { IsNotEmpty, IsString } from 'class-validator';

export class CreateRideDto {
  @IsNotEmpty()
  @IsString()
  current_location: string;
}
