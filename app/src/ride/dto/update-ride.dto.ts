import { PartialType } from '@nestjs/mapped-types';
import { CreateRideDto } from './create-ride.dto';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class UpdateRideDto extends PartialType(CreateRideDto) {
  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  lat: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  long: string;
}
