import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Put,
} from '@nestjs/common';
import { RideService } from './ride.service';
import { CreateRideDto } from './dto/create-ride.dto';
import { UpdateRideDto } from './dto/update-ride.dto';
import { AuthGuard } from '@/guards/auth.guard';

@Controller('rides')
export class RideController {
  constructor(private readonly rideService: RideService) {}

  @UseGuards(AuthGuard)
  @Post('')
  create(@Body() createRideDto: CreateRideDto, @Request() req) {
    return this.rideService.create(createRideDto, req.user);
  }

  @UseGuards(AuthGuard)
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateRideDto: UpdateRideDto,
    @Request() req,
  ) {
    return this.rideService.update(+id, updateRideDto, req.user);
  }
}
