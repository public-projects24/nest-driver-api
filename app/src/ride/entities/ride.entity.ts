import { PaymentMethod } from '@/payment-method/entities/payment-method.entity';
import { RideStatus } from '@/ride-status/entities/ride-status.entity';
import { User } from '@/user/entities/user.entity';

import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

@Entity({ name: 'rides' })
export class Ride {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  initial_location: string;

  @Column({ nullable: true })
  final_location: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @Column()
  rideStatusId: number;

  @ManyToOne(() => RideStatus, (rideStatus) => rideStatus.rides)
  rideStatus: RideStatus;

  @Column()
  driverId: number;

  @ManyToOne(() => User, (user) => user.rides)
  driver: User;

  @Column()
  riderId: number;

  @ManyToOne(() => User, (user) => user.rides)
  rider: User;
}
