import { Module } from '@nestjs/common';
import { RideService } from './ride.service';
import { RideController } from './ride.controller';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '@/auth/constants';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ride } from './entities/ride.entity';
import { HttpModule } from '@nestjs/axios';
import { WompiModule } from '@/wompi/wompi.module';
import { User } from '@/user/entities/user.entity';

@Module({
  imports: [
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      //signOptions: { expiresIn: '60s' },
    }),
    TypeOrmModule.forFeature([Ride, User]),
    HttpModule,
    WompiModule,
  ],
  controllers: [RideController],
  providers: [RideService],
})
export class RideModule {}
