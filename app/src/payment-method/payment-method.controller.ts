import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
} from '@nestjs/common';
import { PaymentMethodService } from './payment-method.service';
import { CreatePaymentMethodDto } from './dto/create-payment-method.dto';
import { AuthGuard } from '@/guards/auth.guard';

@Controller('payments')
export class PaymentMethodController {
  constructor(private readonly paymentMethodService: PaymentMethodService) {}

  @UseGuards(AuthGuard)
  @Post('payment-method')
  create(
    @Body() createPaymentMethodDto: CreatePaymentMethodDto,
    @Request() req,
  ) {
    return this.paymentMethodService.create(createPaymentMethodDto, req.user);
  }
}
