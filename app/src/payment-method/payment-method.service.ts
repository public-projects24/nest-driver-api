import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreatePaymentMethodDto } from './dto/create-payment-method.dto';
import { User } from '@/user/entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpService } from '@nestjs/axios';
import { WompiService } from '@/wompi/wompi.service';
import { PaymentInformation } from '@/payment-information/entities/payment-information.entity';

@Injectable()
export class PaymentMethodService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(PaymentInformation)
    private paymentInformationRepository: Repository<PaymentInformation>,
    private readonly httpService: HttpService,
    private readonly wompiService: WompiService,
  ) {}

  async create(
    createPaymentMethodDto: CreatePaymentMethodDto,
    user: { id: number; email: string },
  ) {
    const userFound = await this.userRepository.findOne({
      where: { id: user.id },
      relations: ['paymentInformation'],
    });

    if (userFound.paymentInformation) {
      return { message: 'this user already has a payment method created.' };
    }

    const acceptenceToken = await this.wompiService.getAcceptenceToken();
    const paymentMethodToken = await this.wompiService.tokenizeCard(
      createPaymentMethodDto,
    );
    const paymentSourceId = await this.wompiService.createPaymentSource(
      user.email,
      paymentMethodToken,
      acceptenceToken,
    );

    let paymentInformation = this.paymentInformationRepository.create({
      token_source_id: paymentSourceId,
      paymentMethodId: 1,
    });

    paymentInformation.user = userFound;
    await this.paymentInformationRepository.save(paymentInformation);

    return {
      message: 'payment method created',
    };
  }
}
