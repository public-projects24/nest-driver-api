import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreatePaymentMethodDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(16)
  number: number;

  @IsNotEmpty()
  @IsString()
  @MaxLength(3)
  cvc: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(2)
  exp_month: number;

  @IsNotEmpty()
  @IsString()
  @MaxLength(2)
  exp_year: number;

  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  card_holder: string;
}
