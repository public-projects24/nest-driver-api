import { CreatePaymentMethodDto } from '@/payment-method/dto/create-payment-method.dto';
import { HttpService } from '@nestjs/axios';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosError } from 'axios';
import { error } from 'console';
import { catchError, firstValueFrom, map, throwError } from 'rxjs';

@Injectable()
export class WompiService {
  private endpoint: string;
  private publicKey: string;
  private privateKey: string;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {
    this.endpoint = this.configService.get('WOMPI_API_URL');
    this.publicKey = this.configService.get('WOMPI_PUBLIC_KEY');
    this.privateKey = this.configService.get('WOMPI_PRIVATE_KEY');
  }

  async getAcceptenceToken() {
    const enpoint = `${this.endpoint}/merchants/${this.publicKey}`;
    const response = await firstValueFrom(this.httpService.get(enpoint));
    return response.data.data.presigned_acceptance.acceptance_token;
  }

  async tokenizeCard(createPaymentMethodDto: CreatePaymentMethodDto) {
    const enpoint = `${this.endpoint}/tokens/cards`;
    const response = await firstValueFrom(
      this.httpService.post(enpoint, createPaymentMethodDto, {
        headers: {
          Authorization: `Bearer ${this.publicKey}`,
        },
      }),
    );
    return response.data.data.id;
  }

  async createPaymentSource(
    userEmail: string,
    paymentMethodToken: string,
    acceptenceToken: string,
  ) {
    const enpoint = `${this.endpoint}/payment_sources`;
    const data = {
      customer_email: userEmail,
      type: 'CARD',
      token: paymentMethodToken,
      acceptance_token: acceptenceToken,
    };
    const response = await firstValueFrom(
      this.httpService.post(enpoint, data, {
        headers: {
          Authorization: `Bearer ${this.privateKey}`,
        },
      }),
    );
    return response.data.data.id;
  }

  async createTransaction(
    totalPrice: number,
    riderEmail: string,
    riderPhoneNumber: string,
    riderFullName: string,
    tokenSourceid: number,
    reference: string,
  ) {
    const enpoint = `${this.endpoint}/transactions`;
    const data = {
      amount_in_cents: totalPrice,
      currency: 'COP',
      customer_email: riderEmail,
      payment_method: {
        installments: 1,
      },
      payment_source_id: tokenSourceid,
      reference: `ref-${reference}`,
      customer_data: {
        phone_number: riderPhoneNumber,
        full_name: riderFullName,
      },
    };
    const response = await firstValueFrom(
      this.httpService.post(enpoint, data, {
        headers: {
          Authorization: `Bearer ${this.privateKey}`,
        },
      }),
    );
    return response.data;
  }
}
