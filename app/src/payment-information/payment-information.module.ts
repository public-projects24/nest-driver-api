import { Module } from '@nestjs/common';
import { PaymentInformationService } from './payment-information.service';
import { PaymentInformationController } from './payment-information.controller';

@Module({
  controllers: [PaymentInformationController],
  providers: [PaymentInformationService],
})
export class PaymentInformationModule {}
