import { PaymentMethod } from '@/payment-method/entities/payment-method.entity';
import { User } from '@/user/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity({ name: 'payment_information' })
export class PaymentInformation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  token_source_id: number;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @Column()
  paymentMethodId: number;

  @ManyToOne(
    () => PaymentMethod,
    (paymentMethod) => paymentMethod.paymentInformation,
  )
  paymentMethod: PaymentMethod;

  @OneToOne(() => User, (user) => user.paymentInformation)
  @JoinColumn()
  user: User;
}
