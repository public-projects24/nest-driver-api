import { PartialType } from '@nestjs/mapped-types';
import { CreatePaymentInformationDto } from './create-payment-information.dto';

export class UpdatePaymentInformationDto extends PartialType(CreatePaymentInformationDto) {}
