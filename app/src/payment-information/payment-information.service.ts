import { Injectable } from '@nestjs/common';
import { CreatePaymentInformationDto } from './dto/create-payment-information.dto';
import { UpdatePaymentInformationDto } from './dto/update-payment-information.dto';

@Injectable()
export class PaymentInformationService {
  create(createPaymentInformationDto: CreatePaymentInformationDto) {
    return 'This action adds a new paymentInformation';
  }

  findAll() {
    return `This action returns all paymentInformation`;
  }

  findOne(id: number) {
    return `This action returns a #${id} paymentInformation`;
  }

  update(id: number, updatePaymentInformationDto: UpdatePaymentInformationDto) {
    return `This action updates a #${id} paymentInformation`;
  }

  remove(id: number) {
    return `This action removes a #${id} paymentInformation`;
  }
}
