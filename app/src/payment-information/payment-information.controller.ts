import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PaymentInformationService } from './payment-information.service';
import { CreatePaymentInformationDto } from './dto/create-payment-information.dto';
import { UpdatePaymentInformationDto } from './dto/update-payment-information.dto';

@Controller('payment-information')
export class PaymentInformationController {
  constructor(private readonly paymentInformationService: PaymentInformationService) {}

  @Post()
  create(@Body() createPaymentInformationDto: CreatePaymentInformationDto) {
    return this.paymentInformationService.create(createPaymentInformationDto);
  }

  @Get()
  findAll() {
    return this.paymentInformationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.paymentInformationService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePaymentInformationDto: UpdatePaymentInformationDto) {
    return this.paymentInformationService.update(+id, updatePaymentInformationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentInformationService.remove(+id);
  }
}
