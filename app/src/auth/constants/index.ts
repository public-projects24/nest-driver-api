export const jwtConstants = {
  secret: `${process.env.NODE_ENV !== 'test' ? process.env.JWT_SECRET : 'abc123'}`,
};
