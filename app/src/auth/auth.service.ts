import {
  HttpException,
  HttpStatus,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { LoginUserDto } from './dto/login-user.dto';
import { UserService } from '@/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from '@/user/dto/create-user.dto';
import { Response } from 'express';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private jwtService: JwtService,
  ) {}

  async register(createUserDto: CreateUserDto, response: Response) {
    const userFound = await this.userService.findByEmail(createUserDto.email);

    if (userFound && Object.keys(userFound).length) {
      throw new HttpException(
        'The email has been taken',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    const user = await this.userService.create(createUserDto);

    const payload = { id: user.id, email: user.email };
    const token = await this.jwtService.signAsync(payload, {
      expiresIn: '1d',
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      expiresIn: '7d',
    });

    response.cookie('jwt', token, {
      httpOnly: true,
      expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000), // 1 day
    });

    response.cookie('refresh-jwt', refreshToken, {
      httpOnly: true,
      expires: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), // 7 days
    });

    return {
      user: user,
      token: token,
      refreshToken: refreshToken,
    };
  }

  async login(loginUserDto: LoginUserDto, response: Response) {
    const user = await this.userService.findByEmail(loginUserDto.email);

    if (!user) {
      throw new UnprocessableEntityException('invalid credentials');
    }

    const isPasswordValid = user.password === loginUserDto.password;
    if (!isPasswordValid) {
      throw new UnprocessableEntityException('password is invalid');
    }

    const payload = { id: user.id, email: user.email };
    const token = await this.jwtService.signAsync(payload, {
      expiresIn: '1d',
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      expiresIn: '7d',
    });

    response.cookie('jwt', token, {
      httpOnly: true,
      expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000), // 1 day
    });

    response.cookie('refresh-jwt', refreshToken, {
      httpOnly: true,
      expires: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), // 7 days
    });

    return {
      user: user,
      token: token,
      refreshToken: refreshToken,
    };
  }

  async refreshToken(user: { id: number; email: string }, response: Response) {
    const payload = { id: user.id, email: user.email };
    const token = await this.jwtService.signAsync(payload, {
      expiresIn: '1d',
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      expiresIn: '7d',
    });

    response.cookie('jwt', token, {
      httpOnly: true,
      expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000), // 1 day
    });

    response.cookie('refresh-jwt', refreshToken, {
      httpOnly: true,
      expires: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), // 7 days
    });

    return {
      user: user,
      token: token,
      refreshToken: refreshToken,
    };
  }
}
