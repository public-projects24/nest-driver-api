import { Transform } from 'class-transformer';
import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
  Max,
  MaxLength,
  Min,
} from 'class-validator';

export class RegisterUserDto {
  @IsEmail()
  @MaxLength(255)
  email: string;

  @IsNotEmpty()
  @Transform(({ value }) => value.trim())
  @IsString()
  @MaxLength(255)
  password: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  @Max(2)
  roleId: number;

  @IsNotEmpty()
  @Transform(({ value }) => value.trim())
  @IsString()
  @MaxLength(255)
  first_name: string;

  @IsNotEmpty()
  @Transform(({ value }) => value.trim())
  @IsString()
  @MaxLength(255)
  last_name: string;

  @IsNotEmpty()
  @Transform(({ value }) => value.trim())
  @IsString()
  @MaxLength(255)
  mobile_phone: string;
}
