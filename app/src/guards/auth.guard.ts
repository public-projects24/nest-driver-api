import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constants';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);

    if (!token) {
      throw new UnauthorizedException();
    }

    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: jwtConstants.secret,
      });
      // 💡 We're assigning the payload to the request object here
      // so that we can access it in our route handlers
      request['user'] = payload;
    } catch {
      throw new UnauthorizedException();
    }

    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const cookies = request.headers.cookie?.split(';') ?? [];
    const searchedCookieJwt = cookies.find((cookie) => {
      if (cookie.trim().startsWith('jwt')) {
        return cookie;
      }
    });

    if (searchedCookieJwt) {
      const [cookieName, cookieValue] = searchedCookieJwt.split('=');
      /* console.log('searchedCookieJwt ', searchedCookieJwt);
      console.log('cookieName ', cookieName);
      console.log('cookieValue ', cookieValue); */

      if (cookieValue) {
        return cookieValue;
      }
    }

    const [type, bearerToken] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? bearerToken : undefined;
  }
}
