import { Test, TestingModule } from '@nestjs/testing';
import { RideStatusService } from './ride-status.service';

describe('RideStatusService', () => {
  let service: RideStatusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RideStatusService],
    }).compile();

    service = module.get<RideStatusService>(RideStatusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
