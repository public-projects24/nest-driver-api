import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RideStatusService } from './ride-status.service';
import { CreateRideStatusDto } from './dto/create-ride-status.dto';
import { UpdateRideStatusDto } from './dto/update-ride-status.dto';

@Controller('ride-status')
export class RideStatusController {
  constructor(private readonly rideStatusService: RideStatusService) {}

  @Post()
  create(@Body() createRideStatusDto: CreateRideStatusDto) {
    return this.rideStatusService.create(createRideStatusDto);
  }

  @Get()
  findAll() {
    return this.rideStatusService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.rideStatusService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRideStatusDto: UpdateRideStatusDto) {
    return this.rideStatusService.update(+id, updateRideStatusDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.rideStatusService.remove(+id);
  }
}
