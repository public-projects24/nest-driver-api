import { Injectable } from '@nestjs/common';
import { CreateRideStatusDto } from './dto/create-ride-status.dto';
import { UpdateRideStatusDto } from './dto/update-ride-status.dto';

@Injectable()
export class RideStatusService {
  create(createRideStatusDto: CreateRideStatusDto) {
    return 'This action adds a new rideStatus';
  }

  findAll() {
    return `This action returns all rideStatus`;
  }

  findOne(id: number) {
    return `This action returns a #${id} rideStatus`;
  }

  update(id: number, updateRideStatusDto: UpdateRideStatusDto) {
    return `This action updates a #${id} rideStatus`;
  }

  remove(id: number) {
    return `This action removes a #${id} rideStatus`;
  }
}
