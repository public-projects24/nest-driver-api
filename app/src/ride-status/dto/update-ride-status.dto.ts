import { PartialType } from '@nestjs/mapped-types';
import { CreateRideStatusDto } from './create-ride-status.dto';

export class UpdateRideStatusDto extends PartialType(CreateRideStatusDto) {}
