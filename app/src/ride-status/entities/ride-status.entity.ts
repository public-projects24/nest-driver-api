import { Ride } from '@/ride/entities/ride.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

export enum RideStatusEnum {
  assigned = 1,
  finished = 2,
}

@Entity({ name: 'ride_statuses' })
export class RideStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @OneToMany(() => Ride, (ride) => ride.rideStatus)
  rides: Ride[];
}
