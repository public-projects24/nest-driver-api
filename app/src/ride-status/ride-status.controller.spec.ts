import { Test, TestingModule } from '@nestjs/testing';
import { RideStatusController } from './ride-status.controller';
import { RideStatusService } from './ride-status.service';

describe('RideStatusController', () => {
  let controller: RideStatusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RideStatusController],
      providers: [RideStatusService],
    }).compile();

    controller = module.get<RideStatusController>(RideStatusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
