import { Module } from '@nestjs/common';
import { RideStatusService } from './ride-status.service';
import { RideStatusController } from './ride-status.controller';

@Module({
  controllers: [RideStatusController],
  providers: [RideStatusService],
})
export class RideStatusModule {}
