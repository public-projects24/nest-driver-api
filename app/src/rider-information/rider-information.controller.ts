import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RiderInformationService } from './rider-information.service';
import { CreateRiderInformationDto } from './dto/create-rider-information.dto';
import { UpdateRiderInformationDto } from './dto/update-rider-information.dto';

@Controller('rider-information')
export class RiderInformationController {
  constructor(private readonly riderInformationService: RiderInformationService) {}

  @Post()
  create(@Body() createRiderInformationDto: CreateRiderInformationDto) {
    return this.riderInformationService.create(createRiderInformationDto);
  }

  @Get()
  findAll() {
    return this.riderInformationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.riderInformationService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRiderInformationDto: UpdateRiderInformationDto) {
    return this.riderInformationService.update(+id, updateRiderInformationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.riderInformationService.remove(+id);
  }
}
