import { PartialType } from '@nestjs/mapped-types';
import { CreateRiderInformationDto } from './create-rider-information.dto';

export class UpdateRiderInformationDto extends PartialType(CreateRiderInformationDto) {}
