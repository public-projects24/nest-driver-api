import { Module } from '@nestjs/common';
import { RiderInformationService } from './rider-information.service';
import { RiderInformationController } from './rider-information.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RiderInformation } from './entities/rider-information.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RiderInformation])],
  controllers: [RiderInformationController],
  providers: [RiderInformationService],
})
export class RiderInformationModule {}
