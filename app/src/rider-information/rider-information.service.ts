import { Injectable } from '@nestjs/common';
import { CreateRiderInformationDto } from './dto/create-rider-information.dto';
import { UpdateRiderInformationDto } from './dto/update-rider-information.dto';

@Injectable()
export class RiderInformationService {
  create(createRiderInformationDto: CreateRiderInformationDto) {
    return 'This action adds a new riderInformation';
  }

  findAll() {
    return `This action returns all riderInformation`;
  }

  findOne(id: number) {
    return `This action returns a #${id} riderInformation`;
  }

  update(id: number, updateRiderInformationDto: UpdateRiderInformationDto) {
    return `This action updates a #${id} riderInformation`;
  }

  remove(id: number) {
    return `This action removes a #${id} riderInformation`;
  }
}
