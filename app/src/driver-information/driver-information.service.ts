import { Injectable } from '@nestjs/common';
import { CreateDriverInformationDto } from './dto/create-driver-information.dto';
import { UpdateDriverInformationDto } from './dto/update-driver-information.dto';

@Injectable()
export class DriverInformationService {
  create(createDriverInformationDto: CreateDriverInformationDto) {
    return 'This action adds a new driverInformation';
  }

  findAll() {
    return `This action returns all driverInformation`;
  }

  findOne(id: number) {
    return `This action returns a #${id} driverInformation`;
  }

  update(id: number, updateDriverInformationDto: UpdateDriverInformationDto) {
    return `This action updates a #${id} driverInformation`;
  }

  remove(id: number) {
    return `This action removes a #${id} driverInformation`;
  }
}
