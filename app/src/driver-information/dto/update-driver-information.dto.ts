import { PartialType } from '@nestjs/mapped-types';
import { CreateDriverInformationDto } from './create-driver-information.dto';

export class UpdateDriverInformationDto extends PartialType(CreateDriverInformationDto) {}
