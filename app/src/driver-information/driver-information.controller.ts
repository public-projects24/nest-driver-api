import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DriverInformationService } from './driver-information.service';
import { CreateDriverInformationDto } from './dto/create-driver-information.dto';
import { UpdateDriverInformationDto } from './dto/update-driver-information.dto';

@Controller('driver-information')
export class DriverInformationController {
  constructor(
    private readonly driverInformationService: DriverInformationService,
  ) {}

  @Post()
  create(@Body() createDriverInformationDto: CreateDriverInformationDto) {
    return this.driverInformationService.create(createDriverInformationDto);
  }

  @Get()
  findAll() {
    return this.driverInformationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.driverInformationService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDriverInformationDto: UpdateDriverInformationDto,
  ) {
    return this.driverInformationService.update(
      +id,
      updateDriverInformationDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.driverInformationService.remove(+id);
  }
}
