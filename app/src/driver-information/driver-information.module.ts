import { Module } from '@nestjs/common';
import { DriverInformationService } from './driver-information.service';
import { DriverInformationController } from './driver-information.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DriverInformation } from './entities/driver-information.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DriverInformation])],
  controllers: [DriverInformationController],
  providers: [DriverInformationService],
})
export class DriverInformationModule {}
