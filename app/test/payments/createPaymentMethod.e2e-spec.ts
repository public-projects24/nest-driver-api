import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { Role } from '@/role/entities/role.entity';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { JwtService } from '@nestjs/jwt';
import { PaymentMethodModule } from '@/payment-method/payment-method.module';
import { HttpService } from '@nestjs/axios';
import { WompiService } from '@/wompi/wompi.service';
import { PaymentInformation } from '@/payment-information/entities/payment-information.entity';

describe('Create payment method (e2e)', () => {
  let app: INestApplication;

  const mockPaymentInformationRepository = {
    create: jest.fn().mockReturnValue({}),
    save: jest.fn().mockReturnValue({}),
  };
  const mockUserRepository = {
    findOne: jest.fn().mockReturnValue({
      email: 'rider@gmail.com',
      password: '123456',
      roleId: 2,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
  };

  const mockHttpService = {
    get: jest.fn().mockReturnValue({}),
    source: {
      subscribe: jest.fn().mockReturnValue({}),
    },
  };

  const mockWompiService = {
    getAcceptenceToken: jest.fn().mockReturnValue('eyJhbGciOiJIUzI1NiJ9'),
    tokenizeCard: jest
      .fn()
      .mockReturnValue('tok_test_10868_8203A23Ccca988ee40cc3119B982e753'),
    createPaymentSource: jest.fn().mockReturnValue(3344),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [PaymentMethodModule],
    })

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockUserRepository)

      .overrideProvider(HttpService)
      .useValue(mockHttpService)

      .overrideProvider(WompiService)
      .useValue(mockWompiService)

      .overrideProvider(getRepositoryToken(PaymentInformation))
      .useValue(mockPaymentInformationRepository)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  it('rider user can create payment method', async () => {
    const jwtService = new JwtService();
    const payload = { id: 1, email: 'rider@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const data = {
      number: '4242424242424242',
      cvc: '789',
      exp_month: '12',
      exp_year: '25',
      card_holder: 'Pedro Pérez',
    };

    const response = await request(app.getHttpServer())
      .post('/payments/payment-method')
      .set('Cookie', ['jwt=' + token])
      .send(data)
      .expect(HttpStatus.CREATED)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.message).toBeDefined();
      });

    return response;
  });

  it('rider user cannot create payment method with empty fields', async () => {
    const jwtService = new JwtService();
    const payload = { id: 1, email: 'rider@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const data = {};

    const response = await request(app.getHttpServer())
      .post('/payments/payment-method')
      .set('Cookie', ['jwt=' + token])
      .send(data)
      .expect(HttpStatus.BAD_REQUEST)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.message).toBeDefined();
      });

    return response;
  });
});
