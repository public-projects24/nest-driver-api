import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { PaymentMethodModule } from '@/payment-method/payment-method.module';
import { HttpService } from '@nestjs/axios';
import { WompiService } from '@/wompi/wompi.service';
import { PaymentInformation } from '@/payment-information/entities/payment-information.entity';
import { RideModule } from '@/ride/ride.module';
import { Ride } from '@/ride/entities/ride.entity';

describe('Finish ride (e2e)', () => {
  let app: INestApplication;

  const mockRideRepository = {
    findOne: jest.fn().mockReturnValue({
      id: 5,
      initial_location: '4.757597868068788,-74.09069447116147',
      final_location: '4.729798446157029,-74.10103706852087',
      created_at: '2024-03-05T22:37:02.000Z',
      updated_at: '2024-03-05T23:37:02.000Z',
      rideStatusId: 1,
      driverId: 1,
      riderId: 15,
    }),
    save: jest.fn().mockReturnValue({
      id: 1,
      initial_location: 'asdf',
      final_location: 'fin lo',
      rideStatusId: 1,
      driverId: 1,
      riderId: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
  };

  const createQueryBuilder: any = {
    select: () => createQueryBuilder,
    innerJoin: () => createQueryBuilder,
    where: () => createQueryBuilder,
    orderBy: () => createQueryBuilder,
    getOne: jest.fn().mockReturnValue({
      id: 6,
      email: 'driver5@gmail.com',
      password: '123456',
      created_at: '2024-03-04T23:02:55.000Z',
      updated_at: '2024-03-04T23:02:55.000Z',
      roleId: 1,
      driverInformation: {
        id: 5,
        first_name: 'fir',
        last_name: 'las',
        mobile_phone: 'phone',
        location: 'loc',
        created_at: '2024-03-04T23:02:55.000Z',
        updated_at: '2024-03-04T23:02:55.000Z',
      },
    }),
  };

  const mockRiderUserRepository = {
    findOne: jest.fn().mockReturnValue({
      email: 'rider@gmail.com',
      password: '123456',
      roleId: 2,
      id: 15,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    createQueryBuilder: jest.fn().mockReturnValue(createQueryBuilder),
  };

  const mockDriverUserRepository = {
    findOneBy: jest.fn().mockReturnValue({
      email: 'driver@gmail.com',
      password: '123456',
      roleId: 1,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    findOne: jest.fn().mockReturnValue({
      id: 15,
      email: 'rider1@gmail.com',
      password: '123456',
      created_at: '2024-03-05T04:38:28.000Z',
      updated_at: '2024-03-05T04:38:28.000Z',
      roleId: 2,
      riderInformation: {
        id: 1,
        first_name: 'fir',
        last_name: 'lasn',
        mobile_phone: '311 2877676',
        location: '-40,-163',
        created_at: '2024-03-05T04:38:28.000Z',
        updated_at: '2024-03-05T04:38:28.000Z',
      },
      paymentInformation: {
        id: 1,
        token_source_id: 101746,
        created_at: '2024-03-05T19:22:23.000Z',
        updated_at: '2024-03-05T19:22:23.000Z',
        paymentMethodId: 1,
      },
    }),
    //createQueryBuilder: jest.fn().mockReturnValue(createQueryBuilder),
  };

  const mockHttpService = {
    get: jest.fn().mockReturnValue({}),
    source: {
      subscribe: jest.fn().mockReturnValue({}),
    },
  };

  const mockWompiService = {
    createTransaction: jest.fn().mockReturnValue({
      data: {
        transactionId: '110868-1709695711-78136',
        transactionStatus: 'PENDING',
      },
    }),
  };

  beforeEach(async () => {});

  it('rider user can finish a ride', async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RideModule],
    })

      .overrideProvider(getRepositoryToken(Ride))
      .useValue(mockRideRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockDriverUserRepository)

      .overrideProvider(HttpService)
      .useValue(mockHttpService)

      .overrideProvider(WompiService)
      .useValue(mockWompiService)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    const jwtService = new JwtService();
    const payload = { id: 1, email: 'driver@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const data = {
      lat: '4.729798446157029',
      long: '-74.10103706852087',
    };

    const response = await request(app.getHttpServer())
      .put(`/rides/${1}`)
      .set('Cookie', ['jwt=' + token])
      .send(data)
      .expect(HttpStatus.OK)
      .expect((response: request.Response) => {
        //console.log(response.body);

        expect(response.body.message).toBeDefined();
        expect(response.body.initial_location).toBeDefined();
        expect(response.body.final_location).toBeDefined();
        expect(response.body.distance).toBeDefined();
        expect(response.body.total_price).toBeDefined();
      });

    return response;
  });

  it('rider user cannot finish a ride with empty location', async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RideModule],
    })

      .overrideProvider(getRepositoryToken(Ride))
      .useValue(mockRideRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockDriverUserRepository)

      .overrideProvider(HttpService)
      .useValue(mockHttpService)

      .overrideProvider(WompiService)
      .useValue(mockWompiService)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    const jwtService = new JwtService();
    const payload = { id: 1, email: 'driver@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const data = {};

    const response = await request(app.getHttpServer())
      .put(`/rides/${1}`)
      .set('Cookie', ['jwt=' + token])
      .send(data)
      .expect(HttpStatus.BAD_REQUEST)
      .expect((response: request.Response) => {
        //console.log(response.body);
      });

    return response;
  });
});
