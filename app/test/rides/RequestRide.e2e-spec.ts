import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { PaymentMethodModule } from '@/payment-method/payment-method.module';
import { HttpService } from '@nestjs/axios';
import { WompiService } from '@/wompi/wompi.service';
import { PaymentInformation } from '@/payment-information/entities/payment-information.entity';
import { RideModule } from '@/ride/ride.module';
import { Ride } from '@/ride/entities/ride.entity';

describe('Request ride (e2e)', () => {
  let app: INestApplication;

  const mockRideRepository = {
    create: jest.fn().mockReturnValue({
      id: 1,
      initial_location: 'asdf',
      final_location: 'fin lo',
      rideStatusId: 1,
      driverId: 1,
      riderId: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    save: jest.fn().mockReturnValue({
      id: 1,
      initial_location: 'asdf',
      final_location: 'fin lo',
      rideStatusId: 1,
      driverId: 1,
      riderId: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
  };

  const createQueryBuilder: any = {
    select: () => createQueryBuilder,
    innerJoin: () => createQueryBuilder,
    where: () => createQueryBuilder,
    orderBy: () => createQueryBuilder,
    getOne: jest.fn().mockReturnValue({
      id: 6,
      email: 'driver5@gmail.com',
      password: '123456',
      created_at: '2024-03-04T23:02:55.000Z',
      updated_at: '2024-03-04T23:02:55.000Z',
      roleId: 1,
      driverInformation: {
        id: 5,
        first_name: 'fir',
        last_name: 'las',
        mobile_phone: 'phone',
        location: 'loc',
        created_at: '2024-03-04T23:02:55.000Z',
        updated_at: '2024-03-04T23:02:55.000Z',
      },
    }),
  };

  const mockRiderUserRepository = {
    findOne: jest.fn().mockReturnValue({
      email: 'rider@gmail.com',
      password: '123456',
      roleId: 2,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    createQueryBuilder: jest.fn().mockReturnValue(createQueryBuilder),
  };

  const mockDriverUserRepository = {
    findOne: jest.fn().mockReturnValue({
      email: 'driver@gmail.com',
      password: '123456',
      roleId: 1,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    createQueryBuilder: jest.fn().mockReturnValue(createQueryBuilder),
  };

  const mockHttpService = {
    get: jest.fn().mockReturnValue({}),
    source: {
      subscribe: jest.fn().mockReturnValue({}),
    },
  };

  const mockWompiService = {
    getAcceptenceToken: jest.fn().mockReturnValue('eyJhbGciOiJIUzI1NiJ9'),
    tokenizeCard: jest
      .fn()
      .mockReturnValue('tok_test_10868_8203A23Ccca988ee40cc3119B982e753'),
    createPaymentSource: jest.fn().mockReturnValue(3344),
  };

  beforeEach(async () => {});

  it('rider user can request a ride', async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RideModule],
    })

      .overrideProvider(getRepositoryToken(Ride))
      .useValue(mockRideRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockRiderUserRepository)

      .overrideProvider(HttpService)
      .useValue(mockHttpService)

      .overrideProvider(WompiService)
      .useValue(mockWompiService)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    const jwtService = new JwtService();
    const payload = { id: 1, email: 'rider@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const data = {
      current_location: '4.757597868068788,-74.09069447116147',
    };

    const response = await request(app.getHttpServer())
      .post('/rides')
      .set('Cookie', ['jwt=' + token])
      .send(data)
      .expect(HttpStatus.CREATED)
      .expect((response: request.Response) => {
        //console.log(response.body);

        expect(response.body.message).toBeDefined();
        expect(response.body.ride).toBeDefined();
        expect(response.body.driver).toBeDefined();
      });

    return response;
  });

  it('driver user cannot request a ride', async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RideModule],
    })

      .overrideProvider(getRepositoryToken(Ride))
      .useValue(mockRideRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockDriverUserRepository)

      .overrideProvider(HttpService)
      .useValue(mockHttpService)

      .overrideProvider(WompiService)
      .useValue(mockWompiService)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    const jwtService = new JwtService();
    const payload = { id: 1, email: 'rider@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const data = {
      current_location: '4.757597868068788,-74.09069447116147',
    };

    const response = await request(app.getHttpServer())
      .post('/rides')
      .set('Cookie', ['jwt=' + token])
      .send(data)
      .expect(HttpStatus.UNAUTHORIZED)
      .expect((response: request.Response) => {
        //console.log(response.body);
      });

    return response;
  });
});
