import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { Role } from '@/role/entities/role.entity';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';

describe('Login (e2e)', () => {
  let app: INestApplication;

  const mockRoleRepository = {};
  const mockRiderInformationRepository = {};
  const mockDriverInformationRepository = {};
  const mockUserRepository = {
    findOneBy: jest.fn().mockResolvedValue({
      email: 'driver@gmail.com',
      password: '123456',
      roleId: 1,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    })

      .overrideProvider(getRepositoryToken(Role))
      .useValue(mockRoleRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockUserRepository)

      .overrideProvider(getRepositoryToken(RiderInformation))
      .useValue(mockRiderInformationRepository)

      .overrideProvider(getRepositoryToken(DriverInformation))
      .useValue(mockDriverInformationRepository)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  it('user can login', async () => {
    const data = {
      email: 'driver3@gmail.com',
      password: '123456',
    };
    const response = await request(app.getHttpServer())
      .post('/auth/login')
      .send(data)
      .expect(HttpStatus.OK)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.user).toBeDefined();
        expect(response.body.token).toBeDefined();
        expect(response.body.refreshToken).toBeDefined();
      });

    const cookies = response.headers['set-cookie'] as unknown as Array<string>;
    const jwt = cookies.filter((cookie) => cookie.startsWith('jwt'));
    const refreshJwt = cookies.filter((cookie) =>
      cookie.startsWith('refresh-jwt'),
    );

    expect(jwt.length).toBe(1);
    expect(refreshJwt.length).toBe(1);

    return response;
  });

  it('user cannot login whitout data', async () => {
    const data = {};
    const response = await request(app.getHttpServer())
      .post('/auth/login')
      .send(data)
      .expect(HttpStatus.BAD_REQUEST)
      .expect((response: request.Response) => {
        //console.log(response.body);
      });
    return response;
  });
});
