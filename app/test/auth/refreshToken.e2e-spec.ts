import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { Role } from '@/role/entities/role.entity';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { JwtService } from '@nestjs/jwt';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';

describe('Refresh Token (e2e)', () => {
  let app: INestApplication;

  const mockRoleRepository = {};
  const mockRiderInformationRepository = {};
  const mockDriverInformationRepository = {};
  const mockUserRepository = {};

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    })

      .overrideProvider(getRepositoryToken(Role))
      .useValue(mockRoleRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockUserRepository)

      .overrideProvider(getRepositoryToken(RiderInformation))
      .useValue(mockRiderInformationRepository)

      .overrideProvider(getRepositoryToken(DriverInformation))
      .useValue(mockDriverInformationRepository)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  it('user can refresh token', async () => {
    const jwtService = new JwtService();
    const payload = { id: 1, email: 'driver@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc123',
      expiresIn: '60s',
    });

    const response = await request(app.getHttpServer())
      .post('/auth/refresh-token')
      .set('Cookie', ['refresh-jwt=' + token])
      .expect(HttpStatus.OK)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.user).toBeDefined();
        expect(response.body.token).toBeDefined();
        expect(response.body.refreshToken).toBeDefined();
      });

    const cookies = response.headers['set-cookie'] as unknown as Array<string>;
    const jwt = cookies.filter((cookie) => cookie.startsWith('jwt'));
    const refreshJwt = cookies.filter((cookie) =>
      cookie.startsWith('refresh-jwt'),
    );

    expect(jwt.length).toBe(1);
    expect(refreshJwt.length).toBe(1);

    return response;
  });

  it('user cannot refresh token with invalid token', async () => {
    const jwtService = new JwtService();
    const payload = { id: 1, email: 'driver@gmail.com' };
    const token = await jwtService.sign(payload, {
      secret: 'abc',
      expiresIn: '60s',
    });

    const response = await request(app.getHttpServer())
      .post('/auth/refresh-token')
      .set('Cookie', ['refresh-jwt=' + token])
      .expect(HttpStatus.UNAUTHORIZED)
      .expect((response: request.Response) => {
        //console.log(response.body);
      });

    return response;
  });

  it('user cannot refresh token without token', async () => {
    const response = await request(app.getHttpServer())
      .post('/auth/refresh-token')
      .expect(HttpStatus.UNAUTHORIZED)
      .expect((response: request.Response) => {
        //console.log(response.body);
      });

    return response;
  });
});
