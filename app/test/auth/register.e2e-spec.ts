import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { Role } from '@/role/entities/role.entity';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';

describe('Register (e2e)', () => {
  let app: INestApplication;

  const mockDriverInformationRepository = {
    create: jest.fn().mockResolvedValue({
      id: 1,
      first_name: 'fir',
      last_name: 'las',
      mobile_phone: 'mob',
      location: 'loc',
      userId: 1,
    }),
    save: jest.fn(),
  };
  const mockRiderInformationRepository = {
    create: jest.fn().mockResolvedValue({
      id: 2,
      first_name: 'fir',
      last_name: 'las',
      mobile_phone: 'mob',
      location: 'loc',
      userId: 2,
    }),
    save: jest.fn(),
  };
  const mockDriverUserRepository = {
    save: jest.fn().mockResolvedValue({
      email: 'driver@gmail.com',
      password: '123456',
      roleId: 1,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    findOneBy: jest.fn().mockReturnValue({}),
  };
  const mockRiderUserRepository = {
    save: jest.fn().mockResolvedValue({
      email: 'rider@gmail.com',
      password: '123456',
      roleId: 2,
      id: 1,
      created_at: '2024-03-04T14:45:06.000Z',
      updated_at: '2024-03-04T14:45:06.000Z',
    }),
    findOneBy: jest.fn().mockReturnValue({}),
  };

  beforeEach(async () => {});

  it('driver user can register', async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    })

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockDriverUserRepository)

      .overrideProvider(getRepositoryToken(DriverInformation))
      .useValue(mockDriverInformationRepository)

      .overrideProvider(getRepositoryToken(RiderInformation))
      .useValue(mockRiderInformationRepository)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    const data = {
      email: 'driver3@gmail.com',
      password: '123456',
      roleId: 1,
      first_name: 'fir',
      last_name: 'las',
      mobile_phone: '311',
    };
    const response = await request(app.getHttpServer())
      .post('/auth/register')
      .send(data)
      .expect(HttpStatus.CREATED)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.user).toBeDefined();
        expect(response.body.token).toBeDefined();
        expect(response.body.refreshToken).toBeDefined();
      });

    const cookies = response.headers['set-cookie'] as unknown as Array<string>;
    const jwt = cookies.filter((cookie) => cookie.startsWith('jwt'));
    const refreshJwt = cookies.filter((cookie) =>
      cookie.startsWith('refresh-jwt'),
    );

    expect(jwt.length).toBe(1);
    expect(refreshJwt.length).toBe(1);

    return response;
  });

  it('rider user can register', async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    })

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockRiderUserRepository)

      .overrideProvider(getRepositoryToken(DriverInformation))
      .useValue(mockDriverInformationRepository)

      .overrideProvider(getRepositoryToken(RiderInformation))
      .useValue(mockRiderInformationRepository)

      .compile();

    app = moduleFixture.createNestApplication();

    app.useGlobalPipes(new ValidationPipe());

    await app.init();

    const data = {
      email: 'rider@gmail.com',
      password: '123456',
      roleId: 2,
      first_name: 'fir',
      last_name: 'las',
      mobile_phone: '311',
    };
    const response = await request(app.getHttpServer())
      .post('/auth/register')
      .send(data)
      .expect(HttpStatus.CREATED)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.user).toBeDefined();
        expect(response.body.token).toBeDefined();
        expect(response.body.refreshToken).toBeDefined();
      });

    const cookies = response.headers['set-cookie'] as unknown as Array<string>;
    const jwt = cookies.filter((cookie) => cookie.startsWith('jwt'));
    const refreshJwt = cookies.filter((cookie) =>
      cookie.startsWith('refresh-jwt'),
    );

    expect(jwt.length).toBe(1);
    expect(refreshJwt.length).toBe(1);

    return response;
  });

  it('user cannot register whitout data', async () => {
    const data = {};
    const response = await request(app.getHttpServer())
      .post('/auth/register')
      .send(data)
      .expect(HttpStatus.BAD_REQUEST)
      .expect((response: request.Response) => {
        //console.log(response.body);
      });
    return response;
  });
});
