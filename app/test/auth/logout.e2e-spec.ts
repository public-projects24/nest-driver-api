import { Test, TestingModule } from '@nestjs/testing';
import {
  ExecutionContext,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '@/auth/auth.module';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '@/user/entities/user.entity';
import { Role } from '@/role/entities/role.entity';
import { DriverInformation } from '@/driver-information/entities/driver-information.entity';
import { RiderInformation } from '@/rider-information/entities/rider-information.entity';

describe('Logout (e2e)', () => {
  let app: INestApplication;

  const mockRoleRepository = {};
  const mockRiderInformationRepository = {};
  const mockDriverInformationRepository = {};
  const mockUserRepository = {};

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    })

      .overrideProvider(getRepositoryToken(Role))
      .useValue(mockRoleRepository)

      .overrideProvider(getRepositoryToken(User))
      .useValue(mockUserRepository)

      .overrideProvider(getRepositoryToken(RiderInformation))
      .useValue(mockRiderInformationRepository)

      .overrideProvider(getRepositoryToken(DriverInformation))
      .useValue(mockDriverInformationRepository)

      .compile();

    app = moduleFixture.createNestApplication();

    await app.init();
  });

  it('user can logout', async () => {
    const data = {};
    const response = await request(app.getHttpServer())
      .post('/auth/logout')
      .send(data)
      .expect(HttpStatus.OK)
      .expect((response: request.Response) => {
        //console.log(response.body);
        expect(response.body.message).toBeDefined();
      });

    const cookies = response.headers['set-cookie'] as unknown as Array<string>;
    const jwt = cookies.filter((cookie) => cookie.startsWith('jwt'));
    const refreshJwt = cookies.filter((cookie) =>
      cookie.startsWith('refresh-jwt'),
    );

    const [jwtName, jwtValue] = jwt[0].split(';')[0].split('=');
    const [refreshJwtName, refreshjwtValue] = refreshJwt[0]
      .split(';')[0]
      .split('=');

    expect(jwtValue).toBe('');
    expect(refreshjwtValue).toBe('');

    return response;
  });
});
