import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateRiderInformationTable1709612409733 implements MigrationInterface {
    name = 'CreateRiderInformationTable1709612409733'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`rider_information\` (\`id\` int NOT NULL AUTO_INCREMENT, \`first_name\` varchar(255) NOT NULL, \`last_name\` varchar(255) NOT NULL, \`mobile_phone\` varchar(255) NOT NULL, \`location\` varchar(255) NOT NULL, \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`userId\` int NULL, UNIQUE INDEX \`REL_f1a6b55637fb9267724d1a42a1\` (\`userId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`rider_information\` ADD CONSTRAINT \`FK_f1a6b55637fb9267724d1a42a15\` FOREIGN KEY (\`userId\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`rider_information\` DROP FOREIGN KEY \`FK_f1a6b55637fb9267724d1a42a15\``);
        await queryRunner.query(`DROP INDEX \`REL_f1a6b55637fb9267724d1a42a1\` ON \`rider_information\``);
        await queryRunner.query(`DROP TABLE \`rider_information\``);
    }

}
