import { MigrationInterface, QueryRunner } from "typeorm";

export class CreatePaymentMethodsTableAndPaymentInformationTable1709665085430 implements MigrationInterface {
    name = 'CreatePaymentMethodsTableAndPaymentInformationTable1709665085430'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`payment_methods\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`payment_information\` (\`id\` int NOT NULL AUTO_INCREMENT, \`token_source_id\` int NULL, \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`paymentMethodId\` int NOT NULL, \`userId\` int NULL, UNIQUE INDEX \`REL_6f9ad54dea7239ae4bbe5abb4b\` (\`userId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`payment_information\` ADD CONSTRAINT \`FK_a8b58d9c983c2388ab4a6cd51e3\` FOREIGN KEY (\`paymentMethodId\`) REFERENCES \`payment_methods\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`payment_information\` ADD CONSTRAINT \`FK_6f9ad54dea7239ae4bbe5abb4b9\` FOREIGN KEY (\`userId\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`payment_information\` DROP FOREIGN KEY \`FK_6f9ad54dea7239ae4bbe5abb4b9\``);
        await queryRunner.query(`ALTER TABLE \`payment_information\` DROP FOREIGN KEY \`FK_a8b58d9c983c2388ab4a6cd51e3\``);
        await queryRunner.query(`DROP INDEX \`REL_6f9ad54dea7239ae4bbe5abb4b\` ON \`payment_information\``);
        await queryRunner.query(`DROP TABLE \`payment_information\``);
        await queryRunner.query(`DROP TABLE \`payment_methods\``);
    }

}
