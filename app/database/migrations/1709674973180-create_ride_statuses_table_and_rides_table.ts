import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateRideStatusesTableAndRidesTable1709674973180 implements MigrationInterface {
    name = 'CreateRideStatusesTableAndRidesTable1709674973180'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`ride_statuses\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`rides\` (\`id\` int NOT NULL AUTO_INCREMENT, \`initial_location\` varchar(255) NOT NULL, \`final_location\` varchar(255) NULL, \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`rideStatusId\` int NOT NULL, \`driverId\` int NOT NULL, \`riderId\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`rides\` ADD CONSTRAINT \`FK_74228fc76b185aa6c7564af5240\` FOREIGN KEY (\`rideStatusId\`) REFERENCES \`ride_statuses\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`rides\` ADD CONSTRAINT \`FK_0adda088d567495e71d21b6c691\` FOREIGN KEY (\`driverId\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`rides\` ADD CONSTRAINT \`FK_3c581fc8082dc803233ec676ef9\` FOREIGN KEY (\`riderId\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`rides\` DROP FOREIGN KEY \`FK_3c581fc8082dc803233ec676ef9\``);
        await queryRunner.query(`ALTER TABLE \`rides\` DROP FOREIGN KEY \`FK_0adda088d567495e71d21b6c691\``);
        await queryRunner.query(`ALTER TABLE \`rides\` DROP FOREIGN KEY \`FK_74228fc76b185aa6c7564af5240\``);
        await queryRunner.query(`DROP TABLE \`rides\``);
        await queryRunner.query(`DROP TABLE \`ride_statuses\``);
    }

}
