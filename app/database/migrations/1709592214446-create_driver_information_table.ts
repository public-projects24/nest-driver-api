import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateDriverInformationTable1709592214446 implements MigrationInterface {
    name = 'CreateDriverInformationTable1709592214446'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`driver_information\` (\`id\` int NOT NULL AUTO_INCREMENT, \`first_name\` varchar(255) NOT NULL, \`last_name\` varchar(255) NOT NULL, \`mobile_phone\` varchar(255) NOT NULL, \`location\` varchar(255) NOT NULL, \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, \`userId\` int NULL, UNIQUE INDEX \`REL_0ba523cb5e065d1d8d315cc358\` (\`userId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`driver_information\` ADD CONSTRAINT \`FK_0ba523cb5e065d1d8d315cc3581\` FOREIGN KEY (\`userId\`) REFERENCES \`users\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`driver_information\` DROP FOREIGN KEY \`FK_0ba523cb5e065d1d8d315cc3581\``);
        await queryRunner.query(`DROP INDEX \`REL_0ba523cb5e065d1d8d315cc358\` ON \`driver_information\``);
        await queryRunner.query(`DROP TABLE \`driver_information\``);
    }

}
