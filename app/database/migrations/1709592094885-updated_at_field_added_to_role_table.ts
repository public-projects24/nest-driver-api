import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdatedAtFieldAddedToRoleTable1709592094885 implements MigrationInterface {
    name = 'UpdatedAtFieldAddedToRoleTable1709592094885'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`roles\` ADD \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`roles\` DROP COLUMN \`updated_at\``);
    }

}
