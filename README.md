## Nest Driver API

**Nest Driver API** is an API for small ride-hailing service, that use the payment plataform.

## A brief explanation

1. A rider logs in with his credencials using the endpoint **"/api/auth/login"**. (this endpoint returns a token)

```bash
email: rider@gmail.com
password: 123456
```

**Note:** The next steps (endpoints) needs that the user (driver or rider) sends the token that was obtained in the login as a bearer token.

2. The rider adds a payment method using the enpoint **"/api/payments/payment-method"**.

```bash
{
  "number": "4242424242424242",
  "cvc": "789",
  "exp_month": "12",
  "exp_year": "29",
  "card_holder": "Pedro Pérez"
}
```

3. The rider request a new ride using the enpoint **"/api/rides"** and sending the curent location

```bash
{
"current_location": "4.757597868068788,-74.09069447116147"
}
```

4. The driver who was assigned to the ride logs in with his credencials using the endpoint **"/api/auth/login"**. (this endpoint returns a token)

```bash
email: email of the driver assigned to the ride
password: 123456
```

5. The driver finish a ride using the endpoint **"/api/rides/{ride_id}"**. Sending the final location (latitude, longitude).

```bash
{
    "lat": "4.729798446157029",
    "long": "-74.10103706852087"
}
```

These are the enpoints availables to complete the flow.

| Method | Endpoint                    | Funcionality                                |
| ------ | --------------------------- | ------------------------------------------- |
| POST   | api/auth/login              | Login for drivers and riders                |
| POST   | api/payments/payment-method | Allows a rider user to add a payment method |
| POST   | /api/rides                  | Allows a rider user request a ride          |
| PUT    | /api/rides/{ride_id}        | Allows a driver user finish a ride          |

## Getting Started

First, you need to have ["Docker"](https://www.docker.com/) installed in your machine
then:

1. Download this repository.
2. Initialize the container and install the npm packages:

```bash
make up
make serv
npm install
```

3. Go to "app" folder and duplicate the ".env.example" with the name ".env"

4. In the .env file set the wompi variables with your credentials.

```bash
WOMPI_API_URL=
WOMPI_PUBLIC_KEY=
WOMPI_PRIVATE_KEY=
```

5. Run the database migration

```bash
npm run migration:r
```

6. Run the development server

```bash
npm run start:dev
```

7. Open [http://localhost:9000/api](http://localhost:9000/api) with your browser. You should see a welcome page.

8. Run de migrations entering to [http://localhost:9000/api/seeders](http://localhost:9000/api/seeders)

9. Run tests

```bash
npm run test:e2e
```

9. Test the API making requests to the enpoints available.
